package com.lab2.benchmarks;

import com.example.domain.Order;
import com.example.repository.ArrayListBasedRepository;
import com.example.repository.HashSetBasedRepository;
import com.example.repository.Repository;
import com.example.repository.TreeSetBasedRepository;
import org.openjdk.jmh.annotations.*;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10,time=1,timeUnit = TimeUnit.SECONDS)
@Measurement(iterations=20,time=1,timeUnit=TimeUnit.SECONDS)
//@Fork(1)
@State(Scope.Benchmark)
public class TreeSetRepository {

    @State(Scope.Thread)
    public static class MyState {
        Repository<Order> repository = new TreeSetBasedRepository();
        int iterations = 100;

        Order order = new Order(150, 4000, 2);
        int orderIndex = -1;

        @Setup(Level.Trial)
        public void setUp() {
            for (int i = 0; i < iterations; i++) {
                repository.add(new Order(i, i + 100, i * 3));
            }

            repository.add(order);
        }
    }

    @Benchmark
    public void testAdd(ArrayListRepositoryBenchmark.MyState state) {
        state.repository.add(new Order(state.iterations + 1, 999,10 ));
    }

    @Benchmark
    public boolean testContains(ArrayListRepositoryBenchmark.MyState state) {
        return state.repository.contains(state.order);
    }

    @Benchmark
    public void testRemove(ArrayListRepositoryBenchmark.MyState state) {
        state.repository.remove(state.order);
    }

    public static void main(String[] args) throws RunnerException {

        Options opt = new OptionsBuilder()
                .include(ArrayListRepositoryBenchmark.class.getSimpleName())
                .forks(1)
                .build();

        new Runner(opt).run();
    }

}
