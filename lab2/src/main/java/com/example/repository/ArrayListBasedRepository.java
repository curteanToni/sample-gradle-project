package com.example.repository;

import com.example.domain.Order;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepository implements Repository<Order>{

    private List<Order> entities;

    public ArrayListBasedRepository(){
        this.entities=new ArrayList<Order>();
    }

    @Override
    public void add(Order entity) {
        entities.add(entity);
    }

    @Override
    public boolean contains(Order entity) {
        return entities.contains(entity);
    }

    @Override
    public void remove(Order entity) {
        entities.remove(entity);
    }
}
