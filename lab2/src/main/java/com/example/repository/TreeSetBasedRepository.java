package com.example.repository;

import com.example.domain.Order;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepository implements Repository<Order>{

    private Set<Order> entities;

    public TreeSetBasedRepository(){
        this.entities=new TreeSet<Order>();
    }

    @Override
    public void add(Order entity) {
        this.entities.add(entity);
    }

    @Override
    public boolean contains(Order entity) {
        return this.entities.contains(entity);
    }

    @Override
    public void remove(Order entity) {
        this.entities.remove(entity);
    }
}
