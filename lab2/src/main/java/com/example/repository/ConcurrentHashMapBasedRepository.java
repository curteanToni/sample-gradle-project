package com.example.repository;

import com.example.domain.Order;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository implements Repository<Order>{

    Map<Integer,Order> entities;

    public ConcurrentHashMapBasedRepository(){
        this.entities=new ConcurrentHashMap<Integer,Order>();
    }


    @Override
    public void add(Order entity) {
        this.entities.put(entity.getId(),entity);
    }

    @Override
    public boolean contains(Order entity) {
        return this.entities.containsValue(entity);
    }

    @Override
    public void remove(Order entity) {
        this.entities.remove(entity.getId(),entity);
    }
}
