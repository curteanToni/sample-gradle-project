package com.example.repository;

public interface Repository<T>{

    void add(T entity);

    boolean contains(T entity);

    void remove(T entity);
}
