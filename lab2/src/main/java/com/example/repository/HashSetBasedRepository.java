package com.example.repository;

import com.example.domain.Order;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepository implements Repository<Order>{

    private Set<Order> entities;

    public HashSetBasedRepository() {
        this.entities = new HashSet<Order>();
    }

    @Override
    public void add(Order entity) {
        entities.add(entity);
    }

    @Override
    public boolean contains(Order entity) {
        return entities.contains(entity);
    }

    @Override
    public void remove(Order entity) {
        entities.remove(entity);
    }
}
