package com.example.repository;

import com.example.domain.Order;
import it.unimi.dsi.fastutil.objects.ObjectArraySet;

import java.util.Set;

public class FastUtilBasedRepository implements Repository<Order>{

    Set<Order> entities;

    public FastUtilBasedRepository(){
        this.entities=new ObjectArraySet<Order>();
    }

    @Override
    public void add(Order entity) {
        this.entities.add(entity);
    }

    @Override
    public boolean contains(Order entity) {
        return this.entities.contains(entity);
    }

    @Override
    public void remove(Order entity) {
        this.entities.remove(entity);
    }
}
