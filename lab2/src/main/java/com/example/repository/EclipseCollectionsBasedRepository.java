package com.example.repository;

import com.example.domain.Order;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

public class EclipseCollectionsBasedRepository implements Repository<Order>{

    private MutableMap<Integer,Order> entities;

    public EclipseCollectionsBasedRepository(){
        this.entities=new UnifiedMap<Integer,Order>();
    }

    @Override
    public void add(Order entity) {
        this.entities.put(entity.getId(),entity);
    }

    @Override
    public boolean contains(Order entity) {
        return this.entities.contains(entity);
    }

    @Override
    public void remove(Order entity) {
        this.entities.remove(entity.getId(),entity);
    }
}
