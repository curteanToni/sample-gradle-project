package com.example.service;

import com.example.domain.*;

import java.util.HashMap;
import java.util.Map;

public class CalculatorService {

     Map<String, Operation> operations;

     public CalculatorService(){
         operations=new HashMap<>();
         this.operations.put("+",new Addition());
         this.operations.put("/",new Division());
         this.operations.put("*",new Multiplication());
         this.operations.put("-",new Subtraction());
         this.operations.put("min",new Min());
         this.operations.put("max",new Max());
     }

     public double doOperation(String operator,double a,double b){
         Operation operation=this.operations.get(operator);
         if(operation==null){
             throw new IllegalArgumentException("This operation is not defined!");
         }
         return operation.calculate(a,b);
     }

}
