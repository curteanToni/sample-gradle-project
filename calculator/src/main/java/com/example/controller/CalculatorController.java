package com.example.controller;

import com.example.service.CalculatorService;

import java.util.Scanner;

public class CalculatorController {

    CalculatorService calculatorService;

    public CalculatorController(CalculatorService calculatorService) {
        this.calculatorService = calculatorService;
    }

    private void menu(){
        System.out.println("For Addition: +");
        System.out.println("For Subtraction: -");
        System.out.println("For Division: /");
        System.out.println("For Multiplication: *");
        System.out.println("For Min: min");
        System.out.println("For max: max");
    }

    public void run(){
        menu();
        Scanner scanner=new Scanner(System.in);
        System.out.println("Enter the first number: ");
        double a=scanner.nextDouble();
        scanner.nextLine();

        System.out.println("Enter the operation: ");
        String operator=scanner.nextLine();

        System.out.println("Enter the second number: ");
        double b=scanner.nextDouble();

        try{
            double res=this.calculatorService.doOperation(operator,a,b);
            System.out.println(res);
        }catch(IllegalArgumentException e){
            System.out.println(e.getMessage());
        }

    }
}
