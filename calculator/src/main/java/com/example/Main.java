package com.example;

import com.example.controller.CalculatorController;
import com.example.service.CalculatorService;

public class Main {
    public static void main(String[] args) {
        CalculatorController calculatorController= new CalculatorController(new CalculatorService());
        calculatorController.run();
    }
}