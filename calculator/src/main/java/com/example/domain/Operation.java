package com.example.domain;

public interface Operation {
    double calculate(double a,double b);

}
